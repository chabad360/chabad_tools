from __future__ import unicode_literals
from frappe import _

def get_data():
    return [
        {
            "label": _("Contact Info"),
            "items": [
                {
                    "type": "doctype",
                    "name": "Contact",
                    "label": _("People"),
                },
                {
                    "type": "doctype",
                    "name": "Household",
                    "label": _("Households"),
                },
            ]
        },
        {
            "label": _("Donations"),
            "items": [
                { 
                    "type": "doctype",
                    "name": "Customers",
                    "label": _("Donors"),
                },
                {
                    "type": "doctype",
                    "name": "Purchase Invoice",
                    "label": _("Pledges"),
                },
                {
                    "type": "doctype",
                    "name": "Payment Entry",
                    "label": _("Donations"),
                },
            ]
        },
        {
            "label": _("Events"),
            "items": [
                {
                    "type": "doctype",
                    "name": "Event"
                }
            ]
        }
    ]

