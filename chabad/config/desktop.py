# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
    return [
        {
            "module_name": "Chabad",
            "color": "grey",
            "icon": "octicon octicon-file-directory",
            "type": "module",
            "label": _("Chabad")
        },
        {
            "module_name": "Household",
            "type": "list",
            "icon": "fa fa-users",
            "_doctype": "Household",
            "color": "#DE2B37",
            "link": "List/Household",
            "label": _("Housholds"), 
        }
    ]
