"""
Configuration for docs
"""

# source_link = "https://gitlab.com/chabad_360/chabad_tools"
# docs_base_url = "https://[org_name].github.io/chabad"
# headline = "Chabad Tools for ERPNext"
# sub_heading = "Chabad Tools for ERPNext is an app to assist with the daily operations of a Chabad Jewish Center"

def get_context(context):
    context.brand_html = "Chabad"
