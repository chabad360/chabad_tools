// Copyright (c) 2019, Mendel Greenberg and contributors
// For license information, please see license.txt

frappe.ui.form.on('Household', {
	refresh: function(frm) {
                frm.toggle_display(['address_html'], !frm.doc.__islocal);

                if(!frm.doc.__islocal) {
                        frappe.contacts.render_address_and_contact(frm);

                } else {
                        frappe.contacts.clear_address_and_contact(frm);
                }
	}
});
