# -*- coding: utf-8 -*-
# Copyright (c) 2019, Mendel Greenberg and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.contacts.address_and_contact import load_address_and_contact

class Household(Document):
	pass

	def onload(self):
		load_address_and_contact(self)

